import com.google.gson.Gson;
import java.util.List;

class Student {
	String name;
	int age;
	List<String> skills;

	@Override
	public String toString() {
		return name + " " + age + " " + skills.toString();
	}
}

class MyTest {
	String s = "test";
	int i = 10;
	int[] j = {1, 2, 3};
	boolean f = true;
}

public class Main {
	public static void main(String[] args) {
		Gson gson = new Gson();
		String s =
				"{\"name\":\"Ivan\"," +
				"\"age\":15," +
				"\"skills\":[\"java\",\"xml\",\"Android\"]}";
		Student student = gson.fromJson(s, Student.class);
		System.out.println(student);
		System.out.println(student.skills.size());

		MyTest t = new MyTest();
		s = gson.toJson(t);
		System.out.println(s);
	}
}
